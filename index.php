<?php
 require('Animal.php');
 require('Frog.php');
 require('Ape.php');

 $sheep = new Animal('Shaun');
 $frog = new Frog('Frog');
 $ape = new Ape('Ape');


 echo "Name : ".$sheep->get_name()."<br>";
 echo "Legs : ".$sheep->get_legs()."<br>";
 echo "Cold Blooded : ".$sheep->get_cold_blooded()."<br>";
 echo "<br>";
 echo "Name : ".$frog->name."<br>";
 echo "Legs : ".$frog->legs."<br>";
 echo "Cold Blooded : ".$frog->cold_blooded."<br>";
 echo "Jump : ".$frog->jump()."<br>";
 echo "<br>";
 echo "Name : ".$ape->name."<br>";
 echo "Legs : ".$ape->legs."<br>";
 echo "Cold Blooded : ".$ape->cold_blooded."<br>";
 echo "Jump : ".$ape->yell()."<br>";
?>